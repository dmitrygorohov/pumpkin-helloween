#include <avr/io.h>
#include <avr/interrupt.h>
#ifdef __AVR_ATtiny45__
#include <avr/power.h>
#endif
#include <avr/sleep.h>

#ifdef _USE_UTIL_DELAY
#include <util/delay.h>
#endif

void wdt_disable();
void wdt_enable();
void shift(uint8_t data, uint8_t _ser_port, uint8_t _ser, uint8_t _clk_port, uint8_t _clk, uint8_t _latch_port, uint8_t _latch);

#define LEYE_HORIZONTAL(X, Y)               shift(X, Y, )

#define WDT_DEFAULT                         (1 << WDP2)   // 0.25 sec

#define LEYE_H_SER_DATA                     PB0
#define LEYE_H_CLK_CLOCK                    PB1
#define LEYE_H_CLK_LATCH                    PB2
#define LEYE_V_SER_DATA                     PB3
#define LEYE_V_CLK_CLOCK                    PB4
#define LEYE_V_CLK_LATCH                    PB5

#define REYE_H_SER_DATA                     PD0
#define REYE_H_CLK_CLOCK                    PD1
#define REYE_H_CLK_LATCH                    PD2
#define REYE_V_SER_DATA                     PD3
#define REYE_V_CLK_CLOCK                    PD4
#define REYE_V_CLK_LATCH                    PD5

#define _HA                                 0 // B segment
#define _HB                                 1 // C segment
#define _HC                                 2 // D segment
#define _HD                                 3 // E segment
#define _HE                                 4 // F segment
#define _HF                                 5 // G segment
#define _HG                                 6 // H segment
#define _HH                                 7 // dot

#ifdef _COMMON_CATODE_
#define SETUP_DIGIT(X)                      (X)         // common catode 7segment indicator
#else
#define SETUP_DIGIT(X)                      0xFF & ~(X) // common anode 7segment indicator
#endif
#define MAKE_LOW(X, Y)                      X &= ~(1 << Y)
#define MAKE_HIGH(X, Y)                     X |= (1 << Y)
#define TOGGLE_BIT(X, Y)                    X ^= (1 << Y)

static inline void _power_sleep() __attribute__((always_inline));
#ifndef _USE_UTIL_DELAY
static inline void _delay_loop_2(uint16_t __count) __attribute__((always_inline));
#define delay_us(cycles) _delay_loop_2(cycles / 2)
#endif

void wdt_disable() {
    cli();
    MCUSR &= ~(1 << WDRF);
    WDTCR |= (1 << WDCE) | (1 << WDE);
    WDTCR = 0x00;
    sei();
}

void wdt_enable(uint8_t mode) {
    cli();
    MCUSR &= ~(1 << WDRF);
    WDTCR |= (1 << WDCE) | (1 << WDE);
    WDTCR = (1 << WDIE) | mode;
    sei();
}

void shift(uint8_t data, uint8_t _ser_port, uint8_t _ser, uint8_t _clk_port, uint8_t _clk, uint8_t _latch_port, uint8_t _latch) {
    for (uint8_t i = 0; i < 8; i++) {
        if (0 == (data & _BV(7 - i))) {
            MAKE_LOW(_ser_port, _ser);
        } else {
            MAKE_HIGH(_ser_port, _ser);
        }
        MAKE_LOW(_clk_port, _clk);
        MAKE_HIGH(_clk_port, _clk);
    }
    MAKE_LOW(_latch_port, _latch);
    MAKE_HIGH(_latch_port, _latch);
}
/*
ISR(PCINT0_vect) {
    cli();
    if (!(PINB & (1 << BUTTON_PIN))) {
    }
    sei();
}
*/
ISR(WDT_vect) {}

int main() {
/*
    DDRB = 0xFF & ~(1 << BUTTON_PIN);
    PORTB = 0x00 | (1 << BUTTON_PIN);
*/
    MAKE_LOW(ADCSRA, ADEN);                 // turn off ADC
    MAKE_LOW(ACSR, ACD);                    // turn off Analog Comparator
#ifdef __AVR_ATtiny45__
    power_adc_disable();
    power_timer0_disable();
    power_timer1_disable();
    power_usi_disable();
#endif
    //MAKE_HIGH(PCMSK, PCINT3);               // enable PCINT3
    sei();
    //MAKE_HIGH(GIMSK, PCIE);                 // enable global pc interrupts

    shift(0xFF);
    while (true) {
        _power_sleep();
    }
    return 0;
}

#ifndef _USE_UTIL_DELAY
void _delay_loop_2(uint16_t __count) {
    __asm__ volatile (
        "1: sbiw %0,1" "\n\t"
        "brne 1b"
            : "=w" (__count)
            : "0" (__count)
    );
}
#endif

void _power_sleep() {
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    cli();
    sleep_enable();
    sleep_bod_disable();
    sei();
    sleep_cpu();
    sleep_disable();
    wdt_enable(WDT_DEFAULT);
}
